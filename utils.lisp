;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :utils)

(defun debug-log (format-string &rest parameters)
  (when (not (log:debug))
    (log:config :debug :nopackage :nofile))
  (let ((message (apply #'format nil format-string parameters)))
    (log:debug message)))

(defun dbg (format-string &rest parameters)
  (apply #'debug-log format-string parameters))

(defun json->list (json)
  (restart-case
      (cl-json:decode-json-from-string json)
    (use-value (value) value)))

(defun make-array-frame (size &optional (el nil) (type t) (simplep nil))
  "All elements points to the same address/reference!"
  (make-array size
              :fill-pointer (if (not simplep) size nil)
              :adjustable (if (not simplep) t nil)
              :initial-element el
              :element-type type))

(defun strip-withespaces (string)
  (let ((re "\\s"))
    (regex-replace re string "")))

(defun remove-unprintable-chars (s)
  (regex-replace-all "\\p{C}" s ""))

(defun unprintable-chars->space (s)
  (regex-replace-all "\\p{C}" s " "))

(define-constant +fnv-prime-32+ 16777619 :test #'=)

(define-constant +fnv-offset-basis-32+ 2166136261 :test #'=)

(defun fnv-hash-32 (octects)
  (let ((hash +fnv-offset-basis-32+))
    (loop for i across octects do
	 (setf hash (boole boole-xor hash i))
	 (setf hash (ldb (byte 32 0) (* hash +fnv-prime-32+))))
    hash))

(defun encode-hash-name (bits)
  (let ((v (utils:make-array-frame 4)))
    (loop for i from 0 below 4 do
         (let ((byte (logand (ash bits (* i -8)) #xff)))
           (setf (elt v i) byte)))
    (regex-replace (format nil "~a+" b64:+padding-char+)
                   (b64:encode v)
                   "a")))

(defun gen-file-name (bytes extension)
  (format nil "~a~a" (encode-hash-name (utils:fnv-hash-32 bytes)) extension))

(defun dump-file-if-does-not-exists (path bytes)
  (when (not (uiop:file-exists-p path))
    (with-open-file (stream
                     path
                     :direction :output
                     :if-does-not-exist :create
                     :if-exists         :error
                     :element-type      '(unsigned-byte 8))
      (write-sequence bytes stream)))
  path)

(defun get-local-hostname ()
  #+sbcl (sb-unix:unix-gethostname)
  #-sbcl (error "get-local-hostname not supported on your compiler"))
