;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :telegram)

(defclass telegram-connection ()
  ((activep
    :initform t
    :initarg  :activep
    :reader   activep
    :writer   (setf activep))))

(defparameter *bot* nil)

(defparameter *telegram-connection* nil)

(defparameter *close-telegram-connection* nil)

(defparameter *telegram-lock* (bt:make-lock "tg-lock"))

(defparameter *telegram-thread* nil)

(defmacro with-valid-chat-id ((update) &body body)
  (with-gensyms (message chat-id)
    `(let* ((,message (access ,update  'message))
            (,chat-id (access ,message 'chat 'id)))
       (when (and ,chat-id
                  (= ,chat-id +tg-chat-id+))
         ,@body))))

(defmacro with-valid-text ((update message text chat-id first-name) &body body)
  `(let* ((,message    (access ,update 'message))
          (,text       (access ,message 'text))
          (,chat-id    (access ,message 'chat 'id))
          (,first-name (access ,message 'from 'first--name)))
     (declare (ignorable ,message ,chat-id ,first-name))
     (if (and ,text
              (string/= ,text "")
              (not (message-from-me-p ,text)))
         (progn ,@body)
         nil)))

(defmacro with-valid-photos ((update message first-name photos) &body body)
  `(let* ((,message    (access ,update  'message))
          (,first-name (access ,message 'from 'first--name))
          (,photos     (access ,message 'photo)))
     (if ,photos
         (progn ,@body)
         nil)))

(defmacro with-valid-document ((update message first-name document) &body body)
  `(let* ((,message    (access ,update  'message))
          (,first-name (access ,message 'from 'first--name))
          (,document   (access ,message 'document)))
     (if ,document
         (progn ,@body)
         nil)))

(defun telegram->irc-fallback (update)
  (if update
      (with-valid-text (update message text chat-id first-name)
        #+debug-mode
        (utils:dbg "-> irc : ~a ~a" chat-id (format-message-for-irc first-name text))
        #-debug-mode
        (message-post :irc (format-message-for-irc first-name text))
        t)
      nil))

(defparameter *telegram->irc-hooks* '())

(defun add-telegram->irc-hook (fn)
  (when (functionp fn)
    (push fn *telegram->irc-hooks*)))

(defun init-bot ()
  (setf *bot* (make-bot *tg-bot-token*)))

(defmacro with-login-to-telegram (&body body)
  `(progn
     ,@body))

(defun format-message-for-irc (uname message)
  (format nil "~a~a ~a"
          +tg-uname-prefix+
          (utils:unprintable-chars->space uname)
          (utils:unprintable-chars->space message)))

(defmacro with-telegram-lock (&body body)
  `(bt:with-lock-held (*telegram-lock*) ,@body))

(defmethod message-from-me-p ((object string)) ; :-(((
  (scan (concatenate 'string "^" +wrap-nick-prefix+) object))

(defun %telegram-thread ()
  ;;(telegram-bot::trace-http)
  (utils:dbg "begin telegram loop")
  (do ()
      ((with-telegram-lock *close-telegram-connection*) nil)
    (with-telegram-lock
        (with-package :telegram
          (let ((updates (get-updates *bot* :timeout +tg-get-update-timeout+)))
            (loop for update across updates do
                 (with-valid-chat-id (update)
                   (let ((hooked (loop
                                    named hooks-loop
                                    for hook in *telegram->irc-hooks* do
                                      (when (funcall hook update)
                                        (return-from hooks-loop t)))))
                     (when (not hooked)
                       (telegram->irc-fallback update))))))))))

(defun init-telegram-thread ()
  (setf *telegram-thread*
        (bt:make-thread #'(lambda ()
                            (%telegram-thread)))))

(defmethod quit-connection ((object telegram-connection))
  (telegram:with-telegram-lock
    (setf *close-telegram-connection* t)))

(defmethod message-post ((destination (eql :telegram)) text)
  (with-telegram-lock
    (tg-bot:send-message *bot* +tg-chat-id+ text)))
