(asdf:defsystem :patetig
  :description "patetig: irc <-> telegram gateway"
  :author "cage <cage@katamail.com>"
  :license "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (:alexandria
               :cl-ppcre-unicode
               :bordeaux-threads
               :log4cl
               :babel
               :cl-json
               :puri
               :drakma
               :birch
               :cl-telegram-bot)
  :components ((:file "package")
	       (:file "config")
               (:file "interfaces")
               (:file "base64-encoding")
               (:file "utils")
               (:file "irc")
               (:file "telegram")
               (:file "wikipedia-irc-hook")
               (:file "image-irc-hook")
               (:file "document-irc-hook")
	       (:file "patetig")))

;;;; uncomment to enable debugging
; (pushnew :debug-mode *features*)
