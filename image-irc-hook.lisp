;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :image-irc-hook)

(defun content-type->extension (ct)
  (cond
    ((string= ct "image/png")
     "png")
    ((string= ct "image/gif")
     "gif")
    ((string= ct "image/jpeg")
     "jpg")
    (t
     "image")))

(defun format-uri-image (image-name)
  (format nil "https://~a/~a" (utils:get-local-hostname) image-name))

(defun image->irc-hook (update)
  (if update
      (with-valid-photos (update message first-name photos)
        (if photos
            (loop for photo across photos do
                 (when-let* ((file-id    (access photo 'telegram::file--id)))
                   (multiple-value-bind (body headers extension)
                       (download-file *bot* file-id)
                     (declare (ignore headers))
                     (let* ((file-name  (gen-file-name (map 'vector #'identity body)
                                                       extension))
                            (local-uri  (format-uri-image file-name))
                            (local-file (format nil "~a~a" +image-output-dir+ file-name)))
                       #+debug-mode
                       (progn
                         (utils:dbg "-> irc : ~a" (format-message-for-irc first-name "image"))
                         (utils:dbg "-> irc : ~a" (format-message-for-irc file-name local-uri))
                         (utils:dbg "write file ~a" local-file))
                       #-debug-mode
                       (progn
                         (dump-file-if-does-not-exists local-file body)
                         (message-post :irc (format-message-for-irc first-name "image"))
                         (message-post :irc (format-message-for-irc first-name local-uri)))
                       (dump-file-if-does-not-exists local-file body)
                       t))))
            nil))
      nil))

;; uncomment to fetch and cache images forwarding a link to IRC channel
; (add-telegram->irc-hook #'image->irc-hook)
