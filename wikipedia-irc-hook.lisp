;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :wikipedia-irc-hook)

(define-constant +wiki-host+ "www.wikipedia.org" :test #'string=)

(defparameter *wiki-api-url-template*
  "https://en.wikipedia.org/w/api.php?action=query&titles=~a&prop=extracts&exintro=&explaintext=&exsentences=1&format=json")

(defun find-wikipedia-summary (keyword)
  (multiple-value-bind (body code)
      (drakma:http-request (format nil *wiki-api-url-template* keyword))
    (when (= code +http-ok+)
      (let* ((cl-json:*json-symbols-package* :wikipedia-irc-hook)
             (response (utils:json->list (babel:octets-to-string  body)))
             (pages    (assoc 'pages (cdr (assoc 'query response)))))
        #+debug-mode (utils:dbg "fetching from wikipedia ~s ~s"
                                (format nil *wiki-api-url-template* keyword) pages)
        (loop for page in (cdr pages) collect
             (let ((title   (cdr (assoc 'title (cdr page))))
                   (extract (cdr (assoc 'extract (cdr page)))))
               (cons title extract)))))))

(defun wikipedia-telegram->irc-hook (update)
  (if update
      (handler-case
          (with-valid-text (update message text chat-id first-name)
            (let ((uri (puri:parse-uri text)))
              (if (and (or (eq (puri:uri-scheme uri) :http)
                           (eq (puri:uri-scheme uri) :https))
                       (string= (puri:uri-host uri) +wiki-host+)
                       (not (null (puri:uri-path uri))))
                  (progn
                    (telegram->irc-fallback update)
                    (let ((pages (find-wikipedia-summary (subseq (puri:uri-path uri) 6))))
                      (loop for page in pages do
                           (let ((summary (format nil "~a, ~a" (car page) (cdr page))))
                             #+debug-mode
                             (utils:dbg "-> irc : ~a ~a"
                                        chat-id
                                        (format-message-for-irc "wikipedia:"
                                                                summary))
                             #-debug-mode
                             (message-post :irc
                                           (format-message-for-irc "wikipedia:"
                                                                    summary))))
                      t))
                  nil)))
        (error () nil))))

(add-telegram->irc-hook #'wikipedia-telegram->irc-hook)
