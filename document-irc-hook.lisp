;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :document-irc-hook)

(defun gen-file-name (bytes extension)
  (format nil "~a~a" (encode-hash-name (utils:fnv-hash-32 bytes)) extension))

(defun format-uri-document (document-name)
  (format nil "https://~a/~a" (utils:get-local-hostname) document-name))

(defun document->irc-hook (update)
  (if update
      (with-valid-document (update message first-name document)
        (if document
            (when-let* ((file-id (access document 'telegram::file--id)))
              (multiple-value-bind (body headers extension)
                  (download-file *bot* file-id)
                (declare (ignore headers))
                (let* ((file-name  (gen-file-name (map 'vector #'identity body)
                                                  extension))
                       (local-uri  (format-uri-document file-name))
                       (local-file (format nil "~a~a" +document-output-dir+ file-name)))
                  #+debug-mode
                  (progn
                    (utils:dbg "-> irc : ~a" (format-message-for-irc first-name "document"))
                    (utils:dbg "-> irc : ~a" (format-message-for-irc file-name local-uri))
                    (utils:dbg "write file ~a" local-file))
                  #-debug-mode
                  (progn
                    (message-post :irc (format-message-for-irc first-name "document"))
                    (message-post :irc (format-message-for-irc first-name local-uri)))
                  (dump-file-if-does-not-exists local-file body)
                  t)))
            nil))
      nil))

;; uncomment to fetch and cache documents forwarding a link to IRC channel
; (add-telegram->irc-hook #'document->irc-hook)
