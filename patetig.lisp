;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :patetig)

(defparameter *main-loop-lock* (bt:make-lock))

(defparameter *stop-main-loop* nil)

(defun main ()
  #-debug-mode (setf *tg-bot-token* (read-line))
  (setf (cl+ssl:ssl-check-verify-p) t)
  (cl+ssl:ssl-set-global-default-verify-paths)
  (when *tg-bot-token*
    (telegram:init-bot)
    (irc:with-login-to-irc
      (irc:with-join-wait
        (irc:init-irc-thread))
      (telegram:init-telegram-thread)
      ;;(message-post :telegram "Hello!")
      (do ()
          ((bt:with-lock-held (*main-loop-lock*)
             *stop-main-loop*)
           t))
      (close-irc)
      (close-telegram))))

(defun close-irc ()
  (quit-connection irc:*irc-connection*)
  (bt:join-thread irc:*irc-thread*)
  (utils:dbg "end thread irc"))

(defun close-telegram ()
  (quit-connection telegram:*telegram-connection*)
  (bt:join-thread telegram:*telegram-thread*)
  (utils:dbg "end thread telegram"))

(defun close-all ()
  (bt:with-lock-held (*main-loop-lock*)
    (setf *stop-main-loop* t)))
