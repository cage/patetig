;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :irc)

(defclass irc-connection (connection) ())

(defparameter *irc-connection* nil)

(defparameter *close-irc-connection* nil)

(defparameter *irc-lock* (bt:make-lock "irc-lock"))

(defparameter *join-cond* (bt:make-condition-variable :name "join-cond"))

(defparameter *join-lock* (bt:make-lock "join-lock"))

(defparameter *irc-thread* nil)

(defmacro with-login-to-irc (&body body)
  `(progn
     (setf *irc-connection* (make-instance 'irc-connection
                                           :server-host +irc-server+
                                           :nick        +irc-username+))
;;;                                       :pass        +irc-password+)))
     (connect *irc-connection*)
     (/join *irc-connection* +irc-channel+)
     ,@body))

(defgeneric format-message-for-telegram (object))

(defmethod format-message-for-telegram ((object privmsg-event))
  (format nil "~a~a ~a ~a"
          +wrap-nick-prefix+
          (nick (user object))
          +wrap-nick-char-end+
          (message object)))

(defmethod message-from-me-p ((object privmsg-event))
  (string= (nick (user object)) +irc-username+))

(defmacro with-irc-lock (&body body)
  `(bt:with-lock-held (*irc-lock*) ,@body))

(defmethod handle-event ((connection irc-connection) (event privmsg-event))
  (with-irc-lock
    (when (not (message-from-me-p event))
      #+debug-mode (utils:dbg "-> telegram : ~a" (format-message-for-telegram event))
      #-debug-mode (message-post :telegram (format-message-for-telegram event)))))


(defmethod handle-message :after ((connection connection) prefix
                                  (command (eql :RPL_NAMREPLY)) params)
  (bt:with-lock-held (*join-lock*)
    #+debug-mode (utils:dbg "received join reply")
    (bt:condition-notify *join-cond*)))

(defun message-loop ()
  "Continuously calls READ-MESSAGE until the connection is closed."
  ;; We keep executing PROCESS-MESSAGE until END-OF-FILE is reached. In that
  ;; case, we check if we wanted to quit, and if not we try to reconnect until
  ;; that succeeds.
  (utils:dbg "begin irc message loop")
  (loop do (handler-case
               (loop (process-message *irc-connection*))
             (end-of-file ()
               (if (activep *irc-connection*)
                   (handler-case
                       (progn (sleep 5)
                              (connect *irc-connection*)
                              t)
                     (serious-condition nil))
                   (loop-finish))))))

(defmacro with-join-wait (&body body)
  `(progn
     (bt:acquire-lock   *join-lock*)
     ,@body
     (bt:condition-wait *join-cond* *join-lock*)
     (bt:release-lock   *join-lock*)))

(defun init-irc-thread ()
  (setf *irc-thread*
        (bt:make-thread #'(lambda ()
                            (message-loop)))))

(defmethod quit-connection ((object irc-connection))
  (with-irc-lock
    (/quit object)
    (setf (activep object) nil)))

(defmethod message-post ((destination (eql :irc)) text)
  (let* ((connection *irc-connection*)
         (channel    (first (channels connection))))
    (/privmsg *irc-connection* channel text)))
