;; patetig: irc <-> telegram gateway
;; Copyright (C) 2017  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :config
  (:use :cl
        :alexandria)
  (:export
   :+irc-username+
   :+irc-password+
   :+irc-server+
   :+irc-channel+
   :+tg-bot-username+
   :*tg-bot-token*
   :+tg-server+
   :+tg-chat-id+
   :+image-output-dir+
   :+image-http-path+
   :+document-output-dir+
   :+document-http-path+
   :+tg-uname-prefix+
   :+tg-get-update-timeout+
   :+wrap-nick-prefix+
   :+wrap-nick-char-end+))

(defpackage :base64-encoding
  (:use :cl
	:alexandria)
  (:nicknames :b64)
  (:export
   :+padding-char+
   :encode
   :decode))

(defpackage :utils
  (:use :cl
        :alexandria
        :cl-ppcre
        :config)
  (:export
   :debug-log
   :dbg
   :json->list
   :make-array-frame
   :strip-withespaces
   :remove-unprintable-chars
   :unprintable-chars->space
   :fnv-hash-32
   :encode-hash-name
   :gen-file-name
   :dump-file-if-does-not-exists
   :get-local-hostname))

(defpackage :interfaces
  (:use :cl
        :config)
  (:export
   :message-from-me-p
   :quit-connection
   :message-post))

(defpackage :irc
  (:use :cl
        :alexandria
        :cl-ppcre
        :birch
        :config
        :interfaces)
  (:export
   :*close-irc-connection*
   :*irc-lock*
   :*irc-connection*
   :*irc-thread*
   :with-login-to-irc
   :with-join-wait
   :format-message-for-telegram
   :handle-event
   :with-irc-lock
   :init-irc-thread))

(defpackage :telegram
  (:use :cl
        :alexandria
        :cl-ppcre
        :cl-telegram-bot
        :config
        :interfaces)
  (:export
   :*close-telegram-connection*
   :*telegram-lock*
   :*telegram-connection*
   :*telegram-thread*
   :*bot*
   :telegram->irc-fallback
   :add-telegram->irc-hook
   :with-valid-text
   :with-valid-photos
   :with-valid-document
   :init-bot
   :with-login-to-telegram
   :format-message-for-irc
   :handle-event
   :with-telegram-lock
   :init-telegram-thread))

(defpackage :wikipedia-irc-hook
  (:use :cl
        :alexandria
        :cl-ppcre
        :cl-telegram-bot
        :config
        :utils
        :interfaces
        :telegram)
  (:export))

(defpackage :image-irc-hook
  (:use :cl
        :alexandria
        :cl-ppcre
        :cl-telegram-bot
        :config
        :utils
        :interfaces
        :telegram)
  (:export))

(defpackage :document-irc-hook
  (:use :cl
        :alexandria
        :cl-ppcre
        :cl-telegram-bot
        :config
        :utils
        :interfaces
        :telegram)
  (:export))

(defpackage :patetig
  (:use :cl
        :alexandria
        :cl-ppcre
        :config
        :interfaces)
  (:export
   :close-all
   :main))
